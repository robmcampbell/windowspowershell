function Edit-Profile {
	start -FilePath "powershell_ise.exe" -ArgumentList "Microsoft.PowerShell_profile.ps1" -WorkingDirectory "C:\Users\rcampbell.INTDOM\Documents\WindowsPowerShell\" -Verb open
}

function Edit-Hosts {
	start -FilePath "notepad++.exe" -ArgumentList "hosts" -WorkingDirectory "C:\Windows\System32\Drivers\etc" -Verb open
}

function Show-Hosts {
	Get-Content C:\Windows\System32\Drivers\etc\hosts
}

function Reload-Profile {
	. C:\Users\rcampbell.INTDOM\Documents\WindowsPowerShell\Microsoft.PowerShell_profile.ps1
}

function Tail-File($path, $n) {
    if ($n > 0) {Get-Content -path $path -wait -tail $n}
    else {Get-Content -path $path -wait}
}

function Set-Location-Error-Logs {
    cd C:\InteliSys\amelia\ErrorLogs
}

function Set-Location-Solutions {
    cd C:\Work\g\amelia\solutions
}

function Set-Location-Devops {
    cd C:\Work\g\amelia-devops
}

function Get-Command-Path($a) {
    (Get-Command $a).Path
}

function Run-Command($a) {
    cmd.exe /c $a
}

function Get-Uptime() {
    (get-date) � (gcim Win32_OperatingSystem).LastBootUpTime
}

Set-Alias l ls
Set-Alias npp notepad++
Set-Alias edprof Edit-Profile
Set-Alias edhosts Edit-Hosts
Set-Alias reloadprof Reload-Profile
Set-Alias tail Tail-File
Set-Alias ErrorLogs Set-Location-Error-Logs
Set-Alias Solutions Set-Location-Solutions
Set-Alias devops Set-Location-Devops
Set-Alias open start
Set-Alias bc bcomp.com
Set-Alias which Get-Command-Path
Set-Alias run Run-Command
Set-Alias hosts Show-Hosts
Set-Alias uptime Get-Uptime

cd ~